package top.xiewenwen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.xiewenwen.util.RedisUtil;

@RestController
@RequestMapping("/redis1")
public class RedisTestController2 {
 
	@Autowired
	private RedisUtil RedisUtil;
	
	@RequestMapping("/getValue")
	public Object test(){
        return RedisUtil.get("myName");
    }
}
